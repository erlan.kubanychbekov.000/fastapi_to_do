# Интернет-платформа для продажи и аренды жилья

## Как установить
Для работы микросервиса нужен Python версии не ниже 3.10 и установленное ПО для контейнеризации - [Docker](https://docs.docker.com/engine/install/).    

Настройка переменных окружения  
1. Скопируйте файл .env.dist в .env
2. Заполните .env файл. Пример:  
```yaml
DATABASE_URL = postgresql://solid:secret@127.0.0.1:5434/home
REDIS_URL = redis://127.0.0.1:6379/0
```

Запуск СУБД Postgresql
```shell
docker run --name to-do-db -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=secret -e POSTGRES_DB=to_do_db -p 5434:5432 -d postgres
```

Установка виртуального окружения для продовой среды на примере ОС Linux
```shell
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```  

### Как удалить контейнеры
СУБД Postgres  
```
 docker rm -f -v solid-db
```

Брокер сообщений REDIS  
```
 docker rm -f -v redis-db
```

## Как запустить web-сервер
Запуск сервера производится в активированном локальном окружение из папки `solid_home/`
```shell
python manage.py runserver
```

