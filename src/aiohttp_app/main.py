from aiohttp import web
from routes import setup_routes
import aiohttp_jinja2
import jinja2
from settings import BASE_DIR
from middlewaers import setup_middlewares

app = web.Application()
aiohttp_jinja2.setup(app,
                     loader=jinja2.FileSystemLoader(str(BASE_DIR / 'aiohttp_app' / 'templates')))
setup_routes(app)
setup_middlewares(app)
web.run_app(app, port=8081)
