from aiohttp import web
import aiohttp_jinja2


@aiohttp_jinja2.template('main.html')
async def main(request):
    response = {"questions": [1, 3]}
    return response


@aiohttp_jinja2.template('register.html')
async def register(request):
    response = {"questions": [1, 3]}
    return response


@aiohttp_jinja2.template('login.html')
async def login(request):
    if request.method == 'POST':
        data = await request.post()

        response = {"questions": [1, 3]}
        return response
