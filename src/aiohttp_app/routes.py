from views import main, register, login


def setup_routes(app):
    app.router.add_get("/", main)
    app.router.add_get("/register", register)
    app.router.add_get('/login', login)
    app.router.add_post('/login', login, name="login")

