from fastapi import FastAPI
from fastapi_users import FastAPIUsers
from src.fast_api.auth.manager import get_user_manager
from src.fast_api.auth.base_config import auth_backend
from src.fast_api.auth.models import User
from src.fast_api.auth.schems import UserRead, UserCreate
from src.fast_api.task.router import router as router_task
from src.fast_api.auth.router import router as router_user

app = FastAPI()

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/jwt",
    tags=["auth"],
)

app.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
    prefix="/auth",
    tags=["auth"],
)

current_user = fastapi_users.current_user()

app.include_router(router_task)
app.include_router(router_user)
