from fastapi import APIRouter
from .service import UserService
from .schems import UserRead
from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession
from src.database import get_async_session
from typing import List

router = APIRouter(prefix="/user")


@router.get("/", response_model=List[UserRead])
async def get_all_user(session: AsyncSession = Depends(get_async_session)):
    user_list = await UserService.get_all_user(session)
    return user_list


@router.get("/{id}", response_model=UserRead)
async def get_user_by_id(id: int, session: AsyncSession = Depends(get_async_session)):
    user = await UserService.get_user_by_id(id, db=session)
    return user
