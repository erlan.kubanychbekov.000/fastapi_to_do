from sqlalchemy import Integer
from fastapi_users.db import SQLAlchemyBaseUserTable
from sqlalchemy.orm import Mapped, mapped_column
from src.database import Base
from sqlalchemy.orm import relationship
from src.database import metadata


class User(SQLAlchemyBaseUserTable[int], Base):
    metadata = metadata
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    tasks = relationship("Task", back_populates="user")

