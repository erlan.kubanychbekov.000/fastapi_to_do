from fastapi_users import FastAPIUsers
from src.fast_api.auth.manager import get_user_manager
from fastapi_users.authentication import BearerTransport, JWTStrategy, AuthenticationBackend
from src.config import SECRET_KEY
from src.fast_api.auth.models import User

bearer_transport = BearerTransport(tokenUrl="auth/jwt/login")


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=SECRET_KEY, lifetime_seconds=3600)


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=bearer_transport,
    get_strategy=get_jwt_strategy,
)

fastapi_users = FastAPIUsers[User, int](
    get_user_manager,
    [auth_backend],
)

current_user = fastapi_users.current_user()
