from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from src.fast_api.auth.models import User
from fastapi import HTTPException


class UserService:
    @staticmethod
    async def get_user_by_id(user_id: int, db: AsyncSession):
        user_result = await db.execute(select(User).where(User.id == user_id))
        user = user_result.scalar_one_or_none()
        if not user:
            raise HTTPException(status_code=404, detail="User not found")
        return user

    @staticmethod
    async def get_all_user(db: AsyncSession):
        user_result = await db.execute(select(User).order_by(User.id))
        users = user_result.scalars().all()
        return users
