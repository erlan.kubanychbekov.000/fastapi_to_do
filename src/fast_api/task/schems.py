from pydantic import BaseModel


class CreateTask(BaseModel):
    title: str
    description: str


class UpdateTask(BaseModel):
    title: str
    description: str
    is_done: bool


class ReadTask(BaseModel):
    id: int
    title: str
    description: str
    is_done: bool
    user_id: int

    class Config:
        orm_mode = True
