from fastapi import APIRouter
from fastapi import Depends
from typing import List
from .service import TaskService
from .schems import CreateTask, ReadTask, UpdateTask
from sqlalchemy.ext.asyncio import AsyncSession
from src.database import get_async_session
from src.fast_api.auth.models import User
from src.fast_api.auth.base_config import current_user

router = APIRouter(
    prefix="/task",
    tags=["Tasks"]
)


@router.get("/list/", response_model=List[ReadTask])
async def get_all_task(session: AsyncSession = Depends(get_async_session)):
    """Get TASK list. Allowed all"""

    query = await TaskService.get_all_task(session)
    return query


@router.get("/{id}", response_model=ReadTask)
async def get_by_id_task(id: int,
                         session: AsyncSession = Depends(get_async_session)
                         ):
    """Get TASK by id. Allowed all"""

    task = await TaskService.get_task(session, id)
    return task


@router.post("/add/", response_model=ReadTask)
async def create_task(new_task: CreateTask,
                      session: AsyncSession = Depends(get_async_session),
                      user: User = Depends(current_user)
                      ):
    """Create TASK only Auth"""

    new_task = await TaskService.create_task(session, data=new_task.dict(), user_id=user.id)
    return new_task


@router.delete('/del/{id}', response_model=bool)
async def delete_task(id: int,
                      session: AsyncSession = Depends(get_async_session),
                      user: User = Depends(current_user)):
    """Delete TASK only Auth"""

    del_task = await TaskService.delete_task(session, id, user.id)
    return del_task


@router.put('/update/{id}', response_model=ReadTask)
async def update_task(id: int,
                      task: UpdateTask,
                      session: AsyncSession = Depends(get_async_session),
                      user: User = Depends(current_user)
                      ):
    """Update TASK Only Auth"""

    updated_task = await TaskService.update_task(session, id, user.id, data=task.dict())
    return updated_task
