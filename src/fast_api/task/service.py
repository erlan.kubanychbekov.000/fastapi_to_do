from src.fast_api.task.models import Task
from sqlalchemy import select, delete, update, and_
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi import HTTPException


class TaskService:
    @staticmethod
    async def get_task(db: AsyncSession, task_id: int):
        """Get TASK by id"""
        try:
            query = select(Task).where(Task.id == task_id)
            result = await db.execute(query)
            task = result.scalar_one_or_none()
            if not task:
                raise HTTPException(status_code=404, detail="Task not found")
        except Exception:
            raise
        return task

    @staticmethod
    async def create_task(db: AsyncSession, data: dict, user_id: int):
        """Create TASK. Authorization only"""
        task = Task(**data)
        task.user_id = user_id
        db.add(task)
        try:
            await db.commit()
        except Exception:
            await db.rollback()
            raise
        return task

    @staticmethod
    async def get_all_task(db: AsyncSession):
        """Get all TASK"""
        query = select(Task).order_by(Task.id)
        data = await db.execute(query)
        return data.scalars().all()

    @staticmethod
    async def update_task(db: AsyncSession, task_id: int, user_id: int, data: dict):
        """Update TASK. Authorization only """
        query = update(Task).where(and_(Task.id == task_id, Task.user_id == user_id)).values(**data)
        await db.execute(query)
        try:
            await db.commit()
        except Exception:
            await db.rollback()
            raise
        task = await TaskService.get_task(db, task_id)
        return task

    @staticmethod
    async def delete_task(db: AsyncSession, task_id: int, user_id: int):
        """Delete TASK. Authorization only"""
        query = delete(Task).where(and_(Task.id == task_id, Task.user_id == user_id))
        result = await db.execute(query)
        affected_rows = result.rowcount
        if affected_rows == 0:
            raise HTTPException(status_code=404, detail="Task not found")
        try:
            await db.commit()
        except Exception:
            await db.rollback()
            raise
        return True
