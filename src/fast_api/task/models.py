from sqlalchemy import Column, Integer, String, ForeignKey, Boolean
from src.fast_api.auth.models import User
from src.database import Base
from sqlalchemy.orm import relationship
from src.database import metadata


class Task(Base):
    __tablename__ = "task"
    metadata = metadata
    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False)
    description = Column(String, nullable=True)
    is_done = Column(Boolean, default=False)
    user_id = Column(ForeignKey(User.id))
    user = relationship(User, back_populates="tasks")

