from httpx import AsyncClient


async def authenticate(ac: AsyncClient, username: str, password: str):
    auth_data = {
        "username": username,
        "password": password
    }
    response = await ac.post("/auth/jwt/login", data=auth_data)
    return response


async def test_user_register(ac: AsyncClient):
    """test register user"""
    data = {
        "email": "user@example.com",
        "password": "string",
    }
    response = await ac.post("/auth/register", json=data)
    assert response.status_code == 201


async def test_login_user(ac: AsyncClient):
    """Test login user by email"""
    response = await authenticate(ac, "user@example.com", "string")
    assert response.status_code == 200


async def test_login_error_user(ac: AsyncClient):
    """Test login user by email"""
    response = await authenticate(ac, "usewwr@example.com", "wwwww")
    assert response.status_code == 400
