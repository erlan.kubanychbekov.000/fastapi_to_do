from httpx import AsyncClient


async def test_get_user_by_id(ac: AsyncClient):
    response = await ac.get("user/1")
    data = response.json()
    assert response.status_code == 200
    assert data["email"] == "user@example.com"
    assert data["is_active"] is True
    assert data["is_superuser"] is False
    assert data["is_verified"] is False
