from httpx import AsyncClient
from test_auth import authenticate


async def test_create_task(ac: AsyncClient):
    """test create task success"""
    data = {
        "title": "test_title",
        "description": "test_descriptions",
    }
    # Authenticate the user and obtain the JWT token
    auth_response = await authenticate(ac, "user@example.com", "string")
    assert auth_response.status_code == 200
    jwt_token = auth_response.json()["access_token"]

    # Add the token to the request headers
    headers = {
        "Authorization": f"Bearer {jwt_token}",
        "Content-Type": "application/json",
    }
    # Send the request with the authenticated headers for create task url
    response = await ac.post("/task/add/", json=data, headers=headers)
    assert response.status_code == 200

    # Send the request without the authenticated headers for create task url
    without_user_response = await ac.post("/task/add/", json=data)
    assert without_user_response.status_code == 401


async def test_get_all_task(ac: AsyncClient):
    """test get all tasks"""
    response = await ac.get("/task/list/")
    assert response.status_code == 200


async def test_update_task(ac: AsyncClient):
    """Test Update TASK"""
    data = {
        "title": "update_title",
        "description": "update_description",
        "is_done": False,
    }
    # Authenticate the user and obtain the JWT token
    auth_response = await authenticate(ac, "user@example.com", "string")
    jwt_token = auth_response.json()["access_token"]
    headers = {
        "Authorization": f"Bearer {jwt_token}",
        "Content-Type": "application/json",
    }
    # Send the request with the authenticated headers for update task url
    response = await ac.put("/task/update/1", json=data, headers=headers)
    assert response.status_code == 200

    # Send the request without the authenticated headers for create task url
    response = await ac.put("/task/update/1", json=data)
    assert response.status_code == 401


async def test_get_by_id_task(ac: AsyncClient):
    """get task by id if status success when response is 200"""
    response = await ac.get("/task/1")
    assert response.status_code == 200
    data = response.json()
    assert data["title"] == "update_title"
    assert data["description"] == "update_description"


async def test_delete_task(ac: AsyncClient):
    """Test Delete TASK"""
    auth_response = await authenticate(ac, "user@example.com", "string")
    jwt_token = auth_response.json()["access_token"]
    headers = {
        "Authorization": f"Bearer {jwt_token}",
        "Content-Type": "application/json",
    }
    # Send the request wit the authenticated headers for delete task url
    response = await ac.delete("/task/del/1", headers=headers)
    assert response.status_code == 200
    # Send the request without the authenticated headers for delete task url
    response = await ac.delete("/task/del/1")
    assert response.status_code == 401
